<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/02/19
 * Time: 20.30
 */

namespace App\Core;

use App\Classes\MessageClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Core
{
    public static function getData($model){

        try{
            $data=$model::all();
            $response= new MessageClass(201,trans('message.201'),$data);
        }catch (\Exception $e){
            $response = new MessageClass(404,trans('message.404'),null);
        }

        return $response->getResponse();

    }

    public static function findOne($id,$model)
    {
        try{
            $decryptedId=decrypt($id);
            $data=$model::find($decryptedId);
            $response = new MessageClass(201,trans('message.201'),$data);
        }catch (\Exception $e){
            $response = new MessageClass(404,trans('message.404'),null);
        }
        return $response->getResponse();

    }

    public static function actionForm($id,$model)
    {

        try{

            $result=self::findOne($id,$model);

            if($result['code']==201){
                $data= $result['data'];
            }else{
                $data = new $model();
            }

            $response = new MessageClass(201, trans('message.201'), $data);
        }catch (\Exception $e){
            $response = new MessageClass(404,trans('message.404'),null);
        }

        return $response->getResponse();

    }

    public static function actionSave($params,$model,$with='',$currentParams=[],$isMany=false)
    {

        DB::beginTransaction();
        try{
            unset($params['_token']);
            $params['id']=decrypt($params['id']);
            $modelName=new $model;
            $rules=$modelName->rules;
            if(is_null($params['id'])){
                $validator = Validator::make($params,$rules);

                if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    $response = new MessageClass(404, $error, null);
                    return $response->getResponse();
                }
                $checkAsFile=self::actionAsFile($rules,'mimes');
                if($checkAsFile){
                    $params=array_merge($params,self::actionUploadImage($checkAsFile,$rules,$params,$modelName->getTable()));
                }
            }else{
                if($modelName->rulesUpdate){
                    $validator = Validator::make($params,$modelName->rulesUpdate);

                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        $response = new MessageClass(404, $error, null);
                        return $response->getResponse();
                    }
                }else{

                    $validator = Validator::make($params,$rules);

                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        $response = new MessageClass(404, $error, null);
                        return $response->getResponse();
                    }

                    $checkAsFile=self::actionAsFile($rules,'mimes');
                    if($checkAsFile){
                        $params=array_merge($params,self::actionUploadImage($checkAsFile,$rules,$params,$modelName->getTable()));
                    }
                }
            }

            if(is_null($params['id'])){
                DB::commit();
                if($with==''){
                    $model::create($params);
                }else{
                    if($isMany==false){
                        $create=$modelName::create($params);
                        $create->$with()->create($currentParams);
                    }else{
                        $create=$modelName::create($params);
                        if(count($currentParams)>0){
                            for($i=0;$i<count($currentParams);$i++){
                                $create->$with()->createMany([$currentParams[$i]]);
                            }
                        }
                    }
                }
                $response = new MessageClass(200, trans('message.200'), null);
            }else{
                DB::commit();
                if($with==''){
                    $update=$model::find($params['id']);
                    //$update->$with()->delete();
                    $update->update($params);
                }else{
                    $update=$model::find($params['id']);
                    if(is_null($update->$with)){
                        $update->$with()->create($currentParams);
                    }else{
                        if($isMany==false){
                            $update->$with()->create($currentParams);
                        }else{
                            $update->$with()->delete();
                            if(count($currentParams)>0){
                                for($i=0;$i<count($currentParams);$i++){
                                    $update->$with()->createMany([$currentParams[$i]]);
                                }
                            }
                        }

                    }
                    $update->update($params);
                }
                $response = new MessageClass(302, trans('message.302'), null);
            }

        }catch (\Exception $e){
            DB::rollBack();
            $response = new MessageClass(500,$e->getMessage(),$e->getMessage());
        }

        return $response->getResponse();


    }


    public static  function actionSaveGetId($params,$model)
    {
        DB::beginTransaction();
        try{
            unset($params['_token']);
            $params['id']=decrypt($params['id']);
            $modelName=new $model;
            $rules=$modelName->rules;

            if(is_null($params['id'])){
                $validator = Validator::make($params,$rules);

                if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    $response = new MessageClass(404, $error, null);
                    return $response->getResponse();
                }
                $checkAsFile=self::actionAsFile($rules,'mimes');
                if($checkAsFile){
                    $params=array_merge($params,self::actionUploadImage($checkAsFile,$rules,$params,$modelName->getTable()));
                }
            }else{
                if($modelName->rulesUpdate){
                    $validator = Validator::make($params,$modelName->rulesUpdate);

                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        $response = new MessageClass(404, $error, null);
                        return $response->getResponse();
                    }
                }else{

                    $validator = Validator::make($params,$rules);

                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        $response = new MessageClass(404, $error, null);
                        return $response->getResponse();
                    }

                    $checkAsFile=self::actionAsFile($rules,'mimes');
                    if($checkAsFile){
                        $params=array_merge($params,self::actionUploadImage($checkAsFile,$rules,$params,$modelName->getTable()));
                    }
                }
            }

            if(is_null($params['id'])){
                DB::commit();
                $params['created_at'] = Carbon::now()->toDateTimeString();
                $params['updated_at'] = Carbon::now()->toDateTimeString();
                $getId=$model::insertGetId($params);
                $response = new MessageClass(200, trans('message.200'), $getId);
            }else{
                DB::commit();
                $model::find($params['id'])->update($params);
                $response = new MessageClass(302, trans('message.302'), $params['id']);
            }

        }catch (\Exception $e){
            DB::rollBack();
            dd($e);
            $response = new MessageClass(500, trans('message.500'),$e->getMessage());
        }

        return $response->getResponse();

    }

    public static function actionDelete($id,$model)
    {

        try{
            $result = self::findOne($id,$model);
            if($result['code'] == 201){
                $data = $result['data'];
                $data->delete();
                $response = new MessageClass(202, trans('message.202'), null);
            }else{
                $response = new MessageClass(405, trans('message.405'), null);
            }

        }catch(\Exception $e){
            $response = new MessageClass(501, trans('message.501'), null);
        }

        return $response->getResponse();

    }

    public static function actionDeleteWhere($conditions=[],$model)
    {

        try{
            $model::where($conditions)->delete();
            $response = new MessageClass(202, trans('message.202'), null);
        }catch(\Exception $e){
            $response = new MessageClass(501, trans('message.501'), null);
        }

        return $response->getResponse();

    }




    public static  function actionFindWhere($conditions=[],$model){

        try{
            $data=$model::where($conditions)->get();

            if(!empty($data)){
                $response= new MessageClass(201,trans('message.201'),$data);
            }else{

                $response= new MessageClass(201,trans('message.404'),$data);
            }

        }catch (\Exception $e){
            $response = new MessageClass(500, trans('message.500'),$e->getMessage());

        }

        return $response->getResponse();

    }


    public static  function actionFindWith($conditions,$model){

        try{
            $data=$model::with($conditions)->get();

            if(!empty($data)){
                $response= new MessageClass(201,trans('message.201'),$data);
            }else{

                $response= new MessageClass(201,trans('message.404'),$data);
            }

        }catch (\Exception $e){
            $response = new MessageClass(500, trans('message.500'),$e->getMessage());

        }

        return $response->getResponse();

    }


    public static function actionInsertArrays($params,$model){

        try{
            $model::insert($params);
            $response = new MessageClass(200, trans('message.200'), null);
        }catch (\Exception $e){
            $response = new MessageClass(500, trans('message.500'),$e->getMessage());

        }

        return $response->getResponse();

    }


    private static function actionAsFile($array,$keyword)
    {
        $data=[];
        foreach($array as $index => $string) {
            if (strpos($string, $keyword) !== FALSE)
                $data[$index]=$string;
        }
        return $data;
    }


    private static function actionUploadImage($data,$rules,$params,$path)
    {
        $fileData=[];
        if(!empty($data)){
            $data=array_intersect($rules,$data);
            foreach ($params as $key =>$value){
                if(array_key_exists($key,$data)){
                    $currentData[$key]=$value;
                }
            }
            foreach ($currentData as $key =>$item){
                if(!is_null($item) || $item!=''){
                    $destinationPath = 'public/uploads/'.$path.'/';
                    if(!file_exists($destinationPath)){
                        mkdir($destinationPath,0777,true);
                    }
                    $fileName = uniqid($key).'_'.date('YmdHis').'_'.$item->getClientOriginalName();
                    $item->move($destinationPath, $fileName);
                    $fileData[$key]=$fileName;
                }
            }

        }
        return $fileData;

    }




}