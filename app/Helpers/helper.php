<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 19/06/19
 * Time: 07.35
 */

function getConfig()
{
    $data=\App\Models\AppConfig::find(1);

    if($data){
        return $data;
    }else{
        return null;
    }
}

function generateMonthName($idx)
{
        if(strlen($idx) < 2){
            $idx = '0'.$idx;
        }
        $months = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11'=> 'November',
            '12' => 'Desember'
        ];

        return $months[$idx];
    }