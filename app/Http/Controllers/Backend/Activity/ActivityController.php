<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/19
 * Time: 06.45
 */

namespace App\Http\Controllers\Backend\Activity;


use App\Http\Controllers\Controller;
use App\Services\ActivityCategoryService;
use App\Services\ActivityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ActivityController extends Controller
{
    private  $activityService;
    private  $activityCategoryService;

    private $titlePage='Kegiatan';
    private $view='backend.activity.activity';


    public function __construct()
    {
        $this->activityService = new  ActivityService();
        $this->activityCategoryService = new  ActivityCategoryService();
    }


    public function index()
    {
        $params=[
            'title' => $this->titlePage
        ];

        return view($this->view.'.index',$params);
    }


    public function add(Request $request)
    {
        $id=$request->input('id');
        $result=$this->activityService->actionForm($id);

        if ($id != 0 || !is_null($id)) {
            $title = "Edit ".$this->titlePage;
        }else{
            $title = "Tambah ".$this->titlePage;
        }
        $activityCategory=$this->activityCategoryService->getData();
        $params=[
            'title'=>$title,
            'data'=>$result['data'],
            'categoryOption'=>$activityCategory['data']
        ];

        return view($this->view.'.form',$params);
    }

    public function save(Request $request)
    {
        $params=$request->all();
        $params['foto']=$request->file('foto');
        $params['created_by_user_id']=Session::get('activeUser')->id;

        $result = $this->activityService->actionSave($params);
        if($result['code'] == 200 || $result['code']==302){
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function  delete(Request $request)
    {
        $id = $request->input('id');
        $result = $this->activityService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->activityService->actionDataTable($request);

    }


}