<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/19
 * Time: 06.48
 */

namespace App\Http\Controllers\Backend\Activity;


use App\Http\Controllers\Controller;
use App\Services\ActivityCategoryService;
use App\Services\DonorCategoryService;
use App\Services\DonorService;
use App\Services\HistoryDonorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HistoryDonorController extends Controller
{
    private $historyDonorService;
    private $donorCategoryService;
    private $donorService;
    private $titlePage='Pemasukan/Donasi';
    private $view='backend.activity.history-donor';


    public function __construct()
    {
        $this->historyDonorService= new HistoryDonorService();
        $this->donorCategoryService= new DonorCategoryService();
        $this->donorService= new DonorService();
    }


    public function index()
    {
        $params=[
            'title' => $this->titlePage
        ];

        return view($this->view.'.index',$params);
    }


    public function add(Request $request)
    {
        $id=$request->input('id');
        $result=$this->historyDonorService->actionForm($id);

        if ($id != 0 || !is_null($id)) {
            $title = "Edit ".$this->titlePage;
        }else{
            $title = "Tambah ".$this->titlePage;
        }
        $donor=$this->donorService->getData();
        $donorCategory=$this->donorCategoryService->getData();
        $params=[
            'title'=>$title,
            'data'=>$result['data'],
            'donorOption'=>$donor['data'],
            'donorCategoryOption'=>$donorCategory['data']
        ];

        return view($this->view.'.form',$params);
    }

    public function save(Request $request)
    {
        $params=$request->all();
        $params['created_by_user_id']=Session::get('activeUser')->id;
        $result = $this->historyDonorService->actionSave($params);
        if($result['code'] == 200 || $result['code']==302){
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function  delete(Request $request)
    {
        $id = $request->input('id');
        $result = $this->historyDonorService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->historyDonorService->actionDataTable($request);

    }
}