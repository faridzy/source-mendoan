<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/19
 * Time: 06.45
 */

namespace App\Http\Controllers\Backend\Activity;


use App\Http\Controllers\Controller;
use App\Services\ActivityCategoryService;
use App\Services\HistoryOutcomeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HistoryOutcomeController extends Controller
{
    private  $historyOutcomeService;
    private $titlePage='Pengeluaran';
    private $view='backend.activity.history-outcome';


    public function __construct()
    {
        $this->historyOutcomeService = new  HistoryOutcomeService();
    }


    public function index()
    {
        $params=[
            'title' => $this->titlePage
        ];

        return view($this->view.'.index',$params);
    }


    public function add(Request $request)
    {
        $id=$request->input('id');
        $result=$this->historyOutcomeService->actionForm($id);

        if ($id != 0 || !is_null($id)) {
            $title = "Edit ".$this->titlePage;
        }else{
            $title = "Tambah ".$this->titlePage;
        }

        $params=[
            'title'=>$title,
            'data'=>$result['data']
        ];

        return view($this->view.'.form',$params);
    }

    public function save(Request $request)
    {
        $params=$request->all();
        $params['proof_of_transaction']=$request->file('proof_of_transaction');
        $params['created_by_user_id']=Session::get('activeUser')->id;
        $result = $this->historyOutcomeService->actionSave($params);
        if($result['code'] == 200 || $result['code']==302){
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function  delete(Request $request)
    {
        $id = $request->input('id');
        $result = $this->historyOutcomeService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->historyOutcomeService->actionDataTable($request);

    }


}