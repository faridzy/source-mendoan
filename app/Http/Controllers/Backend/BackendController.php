<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 14/05/19
 * Time: 08.38
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\Donor;
use App\Models\HistoryDonor;
use App\Models\HistoryOutcome;
use App\Models\Member;
use Illuminate\Support\Facades\DB;

class BackendController extends Controller
{

    private $titlePage='Dashboard';
    private $view='backend.dashboard';



    public function index()
    {
        $params=[
            'title' => $this->titlePage,
            'donorCount'=>Donor::count(),
            'historyDonorCount'=>HistoryDonor::count(),
            'historyOutcomeCount'=>HistoryOutcome::count(),
            'memberCount'=>Member::count(),
            'donorLatest'=>HistoryDonor::orderBy('created_at','DESC')->limit(5)->get(),
        ];

        return view($this->view.'.index',$params);
    }

    public function showGrafik()
    {
        $monthIn  = HistoryDonor::select(
            DB::raw('sum(amount) as sums'),
            DB::raw("DATE_FORMAT(date_input,'%m') as monthKey")
        )
            ->whereYear('created_at', date('Y'))
            ->groupBy('monthKey')
            ->get();

        $monthOut  = HistoryOutcome::select(
            DB::raw('sum(amount) as sums'),
            DB::raw("DATE_FORMAT(date_input,'%m') as monthKey")
        )
            ->whereYear('created_at', date('Y'))
            ->groupBy('monthKey')
            ->get();

        $isIn = [0,0,0,0,0,0,0,0,0,0,0,0];
        $isOut = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($monthIn as $order){
            $isIn[$order->monthKey-1] = (float)$order->sums;
        }
        foreach($monthOut as $item){
            $isOut[$item->monthKey-1] = (float)$item->sums;
        }
        $params=[
            'isIn'=>$isIn,
            'isOut'=>$isOut,
        ];

        return response()->json($params);
    }




}