<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 14/05/19
 * Time: 09.01
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Services\ActivityCategoryService;
use Illuminate\Http\Request;

class ActivityCategoryController extends Controller
{
    private  $activityCategoryService;
    private $titlePage='Kategori Kegiatan';
    private $view='backend.master.activity-category';


    public function __construct()
    {
        $this->activityCategoryService = new  ActivityCategoryService();
    }


    public function index()
    {
        $params=[
            'title' => $this->titlePage
        ];

        return view($this->view.'.index',$params);
    }


    public function add(Request $request)
    {
        $id=$request->input('id');
        $result=$this->activityCategoryService->actionForm($id);

        if ($id != 0 || !is_null($id)) {
            $title = "Edit ".$this->titlePage;
        }else{
            $title = "Tambah ".$this->titlePage;
        }

        $params=[
            'title'=>$title,
            'data'=>$result['data']
        ];

        return view($this->view.'.form',$params);
    }

    public function save(Request $request)
    {
        $params=$request->all();
        $result = $this->activityCategoryService->actionSave($params);
        if($result['code'] == 200 || $result['code']==302){
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function  delete(Request $request)
    {
        $id = $request->input('id');
        $result = $this->activityCategoryService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->activityCategoryService->actionDataTable($request);

    }

}