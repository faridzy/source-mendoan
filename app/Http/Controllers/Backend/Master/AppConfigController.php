<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 19/06/19
 * Time: 06.54
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Services\AppConfigService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AppConfigController extends Controller
{
    private  $appConfigService;
    private $titlePage='Setting';
    private $view='backend.master.config';


    public function __construct()
    {
        $this->appConfigService = new  AppConfigService();
    }


    public function index()
    {
        $data=$this->appConfigService->getData();
        $params=[
            'title' => $this->titlePage,
            'data'=>$data['data']
        ];

        return view($this->view.'.index',$params);
    }


    public function add(Request $request)
    {
        $id=$request->input('id');
        $result=$this->appConfigService->actionForm($id);

        if ($id != 0 || !is_null($id)) {
            $title = "Edit ".$this->titlePage;
        }else{
            $title = "Tambah ".$this->titlePage;
        }

        $params=[
            'title'=>$title,
            'data'=>$result['data']
        ];

        return view($this->view.'.form',$params);
    }

    public function save(Request $request)
    {
        $params=$request->all();
        $params['created_by_user_id']=Session::get('activeUser')->id;
        $params['logo']=$request->file('logo');
        $result = $this->appConfigService->actionSave($params);
        if($result['code'] == 200 || $result['code']==302){
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function  delete(Request $request)
    {
        $id = $request->input('id');
        $result = $this->appConfigService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }




}