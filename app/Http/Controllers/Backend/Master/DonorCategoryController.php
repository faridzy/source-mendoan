<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 14/05/19
 * Time: 09.04
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Services\DonorCategoryService;
use Illuminate\Http\Request;

class DonorCategoryController extends Controller
{
    private  $donorCategoryService;
    private $titlePage='Kategori Donor';
    private $view='backend.master.donor-category';


    public function __construct()
    {
        $this->donorCategoryService = new  DonorCategoryService();
    }


    public function index()
    {
        $params=[
            'title' => $this->titlePage
        ];

        return view($this->view.'.index',$params);
    }


    public function add(Request $request)
    {
        $id=$request->input('id');
        $result=$this->donorCategoryService->actionForm($id);

        if ($id != 0 || !is_null($id)) {
            $title = "Edit ".$this->titlePage;
        }else{
            $title = "Tambah ".$this->titlePage;
        }

        $params=[
            'title'=>$title,
            'data'=>$result['data']
        ];

        return view($this->view.'.form',$params);
    }

    public function save(Request $request)
    {
        $params=$request->all();
        $result = $this->donorCategoryService->actionSave($params);
        if($result['code'] == 200 || $result['code']==302){
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function  delete(Request $request)
    {
        $id = $request->input('id');
        $result = $this->donorCategoryService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->donorCategoryService->actionDataTable($request);

    }

}