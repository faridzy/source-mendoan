<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/05/19
 * Time: 06.07
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Services\MemberService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MemberController extends Controller
{
    private  $memberService;
    private $titlePage='Anggota';
    private $view='backend.master.member';


    public function __construct()
    {
        $this->memberService = new  MemberService();
    }


    public function index()
    {
        $params=[
            'title' => $this->titlePage
        ];

        return view($this->view.'.index',$params);
    }


    public function add(Request $request)
    {
        $id=$request->input('id');
        $result=$this->memberService->actionForm($id);

        if ($id != 0 || !is_null($id)) {
            $title = "Edit ".$this->titlePage;
        }else{
            $title = "Tambah ".$this->titlePage;
        }

        $params=[
            'title'=>$title,
            'data'=>$result['data']
        ];

        return view($this->view.'.form',$params);
    }

    public function save(Request $request)
    {
        $params=$request->all();
        $params['created_by_user_id']=Session::get('activeUser')->id;
        $result = $this->memberService->actionSave($params);
        if($result['code'] == 200 || $result['code']==302){
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function  delete(Request $request)
    {
        $id = $request->input('id');
        $result = $this->memberService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->memberService->actionDataTable($request);

    }

}