<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 19/06/19
 * Time: 16.01
 */

namespace App\Http\Controllers\Backend\Report;


use App\Http\Controllers\Controller;
use App\Models\HistoryDonor;
use App\Models\HistoryOutcome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Excel;

class ReportController extends Controller
{
    private $titlePage='Report';
    private $view='backend.report';



    public function index()
    {
        $params=[
            'title' => $this->titlePage,
        ];

        return view($this->view.'.index',$params);
    }

    public function show(Request $request)
    {
        try{
            $month=$request->month;
            $year=$request->year;
            $monthIn  = HistoryDonor::select(
                DB::raw('sum(amount) as sums'),
                DB::raw("DATE_FORMAT(date_input,'%m') as monthKey")
            )
                ->whereMonth('date_input',$this->generateMonth($month))
                ->whereYear('date_input',$year)
                ->groupBy('monthKey')
                ->get();

            $monthOut  = HistoryOutcome::select(
                DB::raw('sum(amount) as sums'),
                DB::raw("DATE_FORMAT(date_input,'%m') as monthKey")
            )
                ->whereMonth('date_input',$this->generateMonth($month))
                ->whereYear('date_input',$year)
                ->groupBy('monthKey')
                ->get();
            $historyDonor=HistoryDonor::whereMonth('date_input',$this->generateMonth($month))
            ->whereYear('date_input',$year)->get();
            $historyOutcome=HistoryOutcome::whereMonth('date_input',$this->generateMonth($month))
                ->whereYear('date_input',$year)->get();

            $history=[];
            $outcome=[];
            foreach($historyDonor as $item){
                $history[]=[
                    'description'=>$item->description,
                    'date_input'=>$item->date_input,
                    'amount'=>$item->amount,
                    'type'=>0
                ];
            }
            foreach ($historyOutcome as $item2){
                $outcome[]=[
                    'description'=>$item2->outcome_name,
                    'date_input'=>$item2->date_input,
                    'amount'=>$item2->amount,
                    'type'=>1
                ];
            }

            if($month==1 ||$month=='1'){
                $in= $historyDonor=HistoryDonor::whereYear('date_input',$year-1)->sum('amount');
                $out=HistoryOutcome::whereYear('date_input',$year-1)->sum('amount');
                $saldo=$in-$out;
            }else{
                $in=0;
                $out=0;
                for($i=0;$i<$month;$i++){
                    $in+= $historyDonor=HistoryDonor::whereMonth('date_input',$this->generateMonth($i))
                        ->whereYear('date_input',$year)->sum('amount');
                    $out+=HistoryOutcome::whereMonth('date_input',$this->generateMonth($i))
                        ->whereYear('date_input',$year)->sum('amount');
                }
                $saldo=$in-$out;

            }
            $currentData=array_merge($outcome,$history);
            $params=[
                'in'=>$monthIn,
                'out'=>$monthOut,
                'data'=>$currentData,
                'month'=>$month,
                'year'=>$year,
                'saldo'=>$saldo
            ];

            return view($this->view.'.result',$params);

        }catch (\Exception $e){
            return $e;
        }




    }


    private function generateMonth($idx)
    {
        if(strlen($idx) < 2){
            $idx = '0'.$idx;
        }
        return $idx;
    }

    public function excel(Request $request)
    {
        $month=$request->month;
        $year=$request->year;
        $monthIn  = HistoryDonor::select(
            DB::raw('sum(amount) as sums'),
            DB::raw("DATE_FORMAT(date_input,'%m') as monthKey")
        )
            ->whereMonth('date_input',$this->generateMonth($month))
            ->whereYear('date_input',$year)
            ->groupBy('monthKey')
            ->get();

        $monthOut  = HistoryOutcome::select(
            DB::raw('sum(amount) as sums'),
            DB::raw("DATE_FORMAT(date_input,'%m') as monthKey")
        )
            ->whereMonth('date_input',$this->generateMonth($month))
            ->whereYear('date_input',$year)
            ->groupBy('monthKey')
            ->get();
        $historyDonor=HistoryDonor::whereMonth('date_input',$this->generateMonth($month))
            ->whereYear('date_input',$year)->get();
        $historyOutcome=HistoryOutcome::whereMonth('date_input',$this->generateMonth($month))
            ->whereYear('date_input',$year)->get();

        $history=[];
        $outcome=[];
        foreach($historyDonor as $item){
            $history[]=[
                'description'=>$item->description,
                'date_input'=>$item->date_input,
                'amount'=>$item->amount,
                'type'=>0
            ];
        }
        foreach ($historyOutcome as $item2){
            $outcome[]=[
                'description'=>$item2->outcome_name,
                'date_input'=>$item2->date_input,
                'amount'=>$item2->amount,
                'type'=>1
            ];
        }

        if($month==1 ||$month=='1'){
            $in= $historyDonor=HistoryDonor::whereYear('date_input',$year-1)->sum('amount');
            $out=HistoryOutcome::whereYear('date_input',$year-1)->sum('amount');
            $saldo=$in-$out;
        }else{
            $in=0;
            $out=0;
            for($i=0;$i<$month;$i++){
                $in+= $historyDonor=HistoryDonor::whereMonth('date_input',$this->generateMonth($i))
                    ->whereYear('date_input',$year)->sum('amount');
                $out+=HistoryOutcome::whereMonth('date_input',$this->generateMonth($i))
                    ->whereYear('date_input',$year)->sum('amount');
            }
            $saldo=$in-$out;

        }
        $currentData=array_merge($outcome,$history);
        $params=[
            'in'=>$monthIn,
            'out'=>$monthOut,
            'data'=>$currentData,
            'month'=>$month,
            'year'=>$year,
            'saldo'=>$saldo
        ];


        return Excel::create('excel_laporan',function ($excel) use($params){
            $excel->sheet('usulan_laporan',function ($sheet) use($params){
                $sheet->loadView($this->view.'.excel',$params);
            });
        })->export('xls');
    }

}