<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 24/05/19
 * Time: 23.08
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;

class FrontendController extends Controller
{

    private $titlePage='Halaman Depan ';
    private $view='frontend.pages';



    public function index()
    {
        $params=[
            'title' => $this->titlePage
        ];

        return view($this->view.'.index',$params);
    }
}