<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 07/05/19
 * Time: 11.15
 */

namespace App\Http\Controllers;


use App\Services\LoginService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    private $loginService;

    public function __construct()
    {
        $this->loginService = new LoginService();
    }


    public function index()
    {
        if (Session::exists('activeUser')) {
            return redirect('/backend');
        }
        $this->loginService->initializeUser();
        $params=[
            'title'=> 'Login'
        ];

        return view('login.index',$params);
    }

    public function validateLogin(Request $request)
    {
        $email = $request->input('username');
        $password = $request->input('password');

        $params=[
            'username'=>$email,
            'password'=>$password
        ];

        $result = $this->loginService->actionLogin($params);

        if($result['code']== 200){
            Session::put('activeUser',$result['data']);
            return "<div class='alert alert-success' style='text-align: center'>".$result['message']."</div> <script>reload(1000);</script>";
        }else{
            return "<div class='alert alert-warning' style='text-align: center'>".$result['message']."</div>";
        }

    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }

}