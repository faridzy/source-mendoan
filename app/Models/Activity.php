<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.22
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Activity
 * @package App\Models
 */
class Activity extends Model
{
    /**
     * @var string
     */
    protected $table = 'activities';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    protected $fillable=[
        'title',
        'description',
        'foto',
        'activity_category_id',
        'created_by_user_id'
    ];

    public $rules=[
        //'description'=>'required',
        'title'=>'required',
        'activity_category_id'=>'required|exists:activity_categories,id',
        'foto' =>'mimes:jpeg,png,jpg',
        'created_by_user_id'=>'required|exists:users,id'
    ];

    public $rulesUpdate=[
        'description'=>'required',
        'title'=>'required',
        'activity_category_id'=>'required|exists:activity_categories,id',
        'foto' =>'required',
        'created_by_user_id'=>'required|exists:users,id'

    ];


    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'activities.id',
            'title',
            'description',
            'users.username',
            'activity_categories.activity_category_name'
        ])->join('users','users.id','=','activities.created_by_user_id')
            ->join('activity_categories','activity_categories.id','=','activities.activity_category_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getCreated()
    {
        return $this->hasOne('App\Models\User','id','created_by_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getActivityCategory()
    {
        return $this->hasOne('App\Models\ActivityCategory','id','activity_category_id');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getImageActivity()
    {
        return $this->hasMany('App\Models\ImageActivity','activity_id','id');
    }

}