<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.22
 */

namespace App\Models;


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ActivityCategory
 * @package App\Models
 */
class ActivityCategory extends Model
{
    /**
     * @var string
     */
    protected $table = 'activity_categories';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    protected $fillable=[
        'activity_category_name',
    ];

    public $rules=[
        'activity_category_name'=>'required',
    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'activity_category_name'
        ]);
    }

}