<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.22
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class AppConfig
 * @package App\Models
 */
class AppConfig extends Model
{
    /**
     * @var string
     */
    protected $table = 'app_configs';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    protected $fillable=[
        'institution_name',
        'overview',
        'logo',
        'phone_number',
        'email',
        'bank_number_rek',
        'created_by_user_id'
    ];

    public $rules=[
        'institution_name'=>'required',
        'overview'=>'required',
        'logo'=>'mimes:jpeg,png,jpg',
        'phone_number'=>'required',
        'email'=>'required',
        'bank_number_rek'=>'required',
        'created_by_user_id'=>'required|exists:users,id'
    ];

    public $rulesUpdate=[
        'institution_name'=>'required',
        'overview'=>'required',
        'logo'=>'required',
        'phone_number'=>'required',
        'email'=>'required',
        'bank_number_rek'=>'required',
        'created_by_user_id'=>'required|exists:users,id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getCreated()
    {
        return $this->hasOne('App\Models\User','id','created_by_user_id');
    }

}