<?php

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.23
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Donor
 * @package App\Models
 */
class Donor extends Model
{
    /**
     * @var string
     */
    protected $table = 'donors';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    protected $fillable=[
        'name',
        'gender_type',
        'phone_number',
        'address'
    ];

    public $rules=[
        'name'=>'required',
        'gender_type'=>'required',
        'phone_number'=>'required',
        'address'=>'required'
    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'name',
            'gender_type',
            'phone_number',
            'address'
        ]);
    }




}