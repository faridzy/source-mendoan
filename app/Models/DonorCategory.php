<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.22
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DonorCategory
 * @package App\Models
 */
class DonorCategory extends Model
{
    /**
     * @var string
     */
    protected $table = 'donor_categories';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    protected $fillable=[
        'donor_category_name',
    ];

    public $rules=[
        'donor_category_name'=>'required',
    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'donor_category_name'
        ]);
    }
}