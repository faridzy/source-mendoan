<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.23
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HistoryDonor
 * @package App\Models
 */
class HistoryDonor extends Model
{
    /**
     * @var string
     */
    protected $table = 'history_donors';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    protected $fillable=[
        'amount',
        'description',
        'date_input',
        'donor_category_id',
        'donor_id',
        'created_by_user_id'
    ];

    public $rules=[
        'description'=>'required',
        'date_input'=>'required',
        'donor_category_id'=>'required|exists:donor_categories,id',
        'donor_id'=>'required|exists:donors,id',
        'created_by_user_id'=>'required|exists:users,id'
    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'history_donors.id',
            'amount',
            'description',
            'date_input',
            'donors.name',
            'donor_categories.donor_category_name'
        ])->join('donors','donors.id','=','history_donors.donor_id')
            ->join('donor_categories','donor_categories.id','=','history_donors.donor_category_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getDonorCategory()
    {
        return $this->hasOne('App\Models\DonorCategory','id','donor_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getDonor()
    {
        return $this->hasOne('App\Models\Donor','id','donor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getCreated()
    {
        return $this->hasOne('App\Models\User','id','created_by_user_id');
    }
}