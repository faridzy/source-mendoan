<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.23
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class HistoryOutcomeController
 * @package App\Models
 */
class HistoryOutcome extends Model
{
    /**
     * @var string
     */
    protected $table = 'history_outcomes';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    protected $fillable=[
        'amount',
        'outcome_name',
        'date_input',
        'proof_of_transaction',
        'created_by_user_id'
    ];

    public $rules=[
        'outcome_name'=>'required',
        'date_input'=>'required',
        'proof_of_transaction' =>'mimes:jpeg,png,jpg',
        'created_by_user_id'=>'required|exists:users,id'
    ];

    public $rulesUpdate=[
        'outcome_name'=>'required',
        'date_input'=>'required',
        'proof_of_transaction' =>'required',
        'created_by_user_id'=>'required|exists:users,id'

    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'history_outcomes.id',
            'amount',
            'outcome_name',
            'date_input',
            'users.username'
        ])->join('users','users.id','=','history_outcomes.created_by_user_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getCreated()
    {
        return $this->hasOne('App\Models\User','id','created_by_user_id');
    }

}