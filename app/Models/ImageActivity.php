<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.42
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ImageActivity extends Model
{
    /**
     * @var string
     */
    protected $table = 'image_activities';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getRole()
    {
        return $this->hasOne('App\Models\Activity','id','activity_id');
    }

}