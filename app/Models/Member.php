<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.23
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Member
 * @package App\Models
 */
class Member extends Model
{
    /**
     * @var string
     */
    protected $table = 'members';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */


    protected $fillable=[
        'fullname',
        'birth_date',
        'birth_place',
        'address',
        'member_type',
        'gender_type',
        'phone_number',
        'created_by_user_id'
    ];

    public $rules=[
        'fullname'=>'required',
        'birth_date'=>'required',
        'birth_place'=>'required',
        'address'=>'required',
        'gender_type'=>'required',
        'created_by_user_id'=>'required|exists:users,id',
    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'fullname',
            'birth_date',
            'birth_place',
            'address',
            'member_type',
            'gender_type',
            'phone_number'
        ]);
    }
    public function getCreated()
    {
        return $this->hasOne('App\Models\User','id','created_by_user_id');
    }

}