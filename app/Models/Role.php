<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.21
 */

namespace App\Models;


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App\Models
 */
class Role extends Model
{
    /**
     * @var string
     */
    protected $table = 'roles';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    protected $fillable=[
        'role_name',
    ];

    public $rules=[
        'role_name'=>'required',
    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'role_name'
        ]);
    }


}