<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 06/05/19
 * Time: 22.22
 */

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App\Models
 */
class User extends Model
{
    /**
     * @var string
     */
    protected $table = 'users';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */

    protected $fillable=[
        'username',
        'password',
        'role_id',
    ];

    public $rules=[
        'username'=>'required',
        'password'=>'required',
        'role_id'=>'required|exists:roles,id',
    ];


    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'users.id',
            'username',
            'roles.role_name',
        ])->join('roles','roles.id','=','users.role_id');
    }


    public function getRole()
    {
        return $this->hasOne('App\Models\Role','id','role_id');
    }



}