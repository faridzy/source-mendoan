<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 19/06/19
 * Time: 02.32
 */

namespace App\Services;


use App\Core\Core;
use App\Models\Activity;
use Yajra\DataTables\DataTables;

class ActivityService
{
    const MODEL_NAME=Activity::class;

    public function getData()
    {
        return Core::getData(self::MODEL_NAME);
    }

    public function findOne($id)
    {

        return Core::findOne($id,self::MODEL_NAME);
    }

    public function actionForm($id)
    {
        return Core::actionForm($id,self::MODEL_NAME);
    }

    public function actionSave($params)
    {
        $checking=$this->findOne($params['id']);

        if(is_null($checking['data'])){
            return Core::actionSaveGetId($params,self::MODEL_NAME);
        }else{

            if(is_null($params['foto']) || $params['foto']==''){
                $params['foto']=$checking['data']->foto;
                return Core::actionSaveGetId($params,self::MODEL_NAME);
            }else{
                return Core::actionSaveGetId($params,self::MODEL_NAME);
            }
        }
    }
    public function actionDelete($id)
    {
        return Core::actionDelete($id,self::MODEL_NAME);

    }

    public function actionDataTable($params)
    {
        $model=self::MODEL_NAME;
        $data=$model::dataTable();
        $dataTables = DataTables::of($data)
            ->addColumn('action', function ($list) {
                return "<a class='btn btn-sm btn-primary' onclick='loadModal(this)' target='/backend/activity/activity/add' data='id=".encrypt($list->id)."' title='Edit'>
                                                    <span class='os-icon os-icon-pencil-1' style='color: white'></span>
                                                </a>
                                                <a class='btn btn-sm btn-danger' onclick=deleteData('".encrypt($list->id)."') title='Hapus'>
                                                    <span class='os-icon os-icon-trash' style='color: white'></span>
                                                </a>";
            })
            ->filter(function ($query) use ($params) {
                if ($params->input('search')['value']) {
                    $query->where('title', 'like', "%{$params->input('search')['value']}%");
                }
            })
            ->setRowId('id');



        return $dataTables->make(true);
    }

}