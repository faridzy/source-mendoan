<?php
/**
 * Copyright (c) 2019. Faridzy Labs
 */

/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 07/05/19
 * Time: 11.26
 */

namespace App\Services;


use App\Classes\MessageClass;
use App\Models\User;
use App\Models\Role;

class LoginService
{
    public function actionLogin($params)
    {
        $username = $params['username'];
        $password = $params['password'];

        if (is_null($username) || $username == "") {
            $response = new MessageClass(404, 'Parameter email tidak tersedia', null);
            return $response->getResponse();
        }

        if (is_null($password) || $password == "") {
            $response = new MessageClass(404, 'Parameter password tidak tersedia', null);
            return $response->getResponse();
        }

        $activeUser = User::where(['username' => $username])->first();
        if (!is_null($activeUser)) {
            if ($activeUser->password != sha1($password)) {
                $response = new MessageClass(404, 'Password tidak sesuai', null);
                return $response->getResponse();
            } elseif ($activeUser->is_active != 1) {
                $response = new MessageClass(404, 'Pengguna sedang dinonaktifkan', null);
                return $response->getResponse();
            } else {
                $response = new MessageClass(200, 'Proses authentifikasi berhasil', $activeUser);
                return $response->getResponse();
            }
        } else {
            $response = new MessageClass(404, 'Username tidak valid', null);
            return $response->getResponse();
        }


    }

    public function initializeUser()
    {

        //Inisialisasi pengguna
        $user = User::count();
        if ($user == 0) {
            //Inisialisasi roles
            $role = Role::count();
            if ($role == 0) {
                $role = new Role();
                $role->id = 1;
                $role->role_name = "Administrator";
                $role->save();
            } else {
                $role = Role::orderBy('id', 'ASC')->first();
            }

            //Inisialisasi pengguna
            $data = new User();
            $data->username = "admin";
            $data->password = sha1("admin");
            $data->role_id = $role->id;
            try {
                $data->save();
            } catch (\Exception $e) {

            }


        }
    }
}