<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryDonorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_donors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('amount')->default(0);
            $table->text('description')->nullable();
            $table->date('date_input');
            $table->tinyInteger('donor_type')->default(0);
            $table->bigInteger('donor_category_id')->unsigned();
            $table->bigInteger('donor_id')->unsigned();
            $table->bigInteger('created_by_user_id')->unsigned();
            $table->foreign('donor_category_id')->references('id')->on('donor_categories');
            $table->foreign('donor_id')->references('id')->on('donors');
            $table->foreign('created_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_donors');
    }
}
