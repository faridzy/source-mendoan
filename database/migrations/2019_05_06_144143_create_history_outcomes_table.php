<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryOutcomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_outcomes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('outcome_name');
            $table->date('date_input');
            $table->string('deskription')->nullable();
            $table->string('proof_of_transaction')->nullable();
            $table->double('amount')->default(0);
            $table->bigInteger('created_by_user_id')->unsigned();
            $table->foreign('created_by_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_outcomes');
    }
}
