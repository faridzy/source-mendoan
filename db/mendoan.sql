-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 19 Jun 2019 pada 02.56
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mendoan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `activities`
--

CREATE TABLE `activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_realization` tinyint(4) NOT NULL DEFAULT '0',
  `activity_category_id` bigint(20) UNSIGNED NOT NULL,
  `created_by_user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `activities`
--

INSERT INTO `activities` (`id`, `title`, `foto`, `description`, `is_realization`, `activity_category_id`, `created_by_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Highcharts Demo', 'foto5d0944d1c0e27_20190618200849_balasan-magang (1).jpeg', '<p>ammamamamwm</p>', 0, 1, 1, '2019-06-18 13:08:49', '2019-06-18 13:08:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `activity_categories`
--

CREATE TABLE `activity_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `activity_category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `activity_categories`
--

INSERT INTO `activity_categories` (`id`, `activity_category_name`, `created_at`, `updated_at`) VALUES
(1, 'Events', '2019-06-18 13:04:38', '2019-06-18 17:52:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_configs`
--

CREATE TABLE `app_configs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `institution_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_number_rek` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `app_configs`
--

INSERT INTO `app_configs` (`id`, `institution_name`, `overview`, `address`, `logo`, `phone_number`, `email`, `bank_number_rek`, `created_by_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Ashabul Kahfi', 'Merupakan panti asuhan yang ada Di Surabaya', 'Nusa Indah No 12', 'logo5d0982af9bef6_20190619003247_user.png', '089644223272', 'admin@admin.com', 'Mandiri: 1234567817981', 1, '2019-06-18 17:32:47', '2019-06-18 17:32:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `donors`
--

CREATE TABLE `donors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender_type` tinyint(4) NOT NULL DEFAULT '0',
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `donors`
--

INSERT INTO `donors` (`id`, `name`, `address`, `gender_type`, `phone_number`, `created_at`, `updated_at`) VALUES
(1, 'Muhammad Farid Habiburrohim', 'Nusa Indah No 12', 1, '089644223272', '2019-06-18 11:55:37', '2019-06-18 11:55:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `donor_categories`
--

CREATE TABLE `donor_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `donor_category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `donor_categories`
--

INSERT INTO `donor_categories` (`id`, `donor_category_name`, `created_at`, `updated_at`) VALUES
(1, 'Uang', '2019-05-24 07:57:29', '2019-05-24 07:57:29'),
(2, 'Barang', '2019-05-24 07:57:39', '2019-05-24 07:57:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_donors`
--

CREATE TABLE `history_donors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `date_input` date NOT NULL,
  `donor_type` tinyint(4) NOT NULL DEFAULT '0',
  `donor_category_id` bigint(20) UNSIGNED NOT NULL,
  `donor_id` bigint(20) UNSIGNED NOT NULL,
  `created_by_user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `history_donors`
--

INSERT INTO `history_donors` (`id`, `amount`, `description`, `date_input`, `donor_type`, `donor_category_id`, `donor_id`, `created_by_user_id`, `created_at`, `updated_at`) VALUES
(1, 200000, 'amamam', '2019-06-30', 0, 1, 1, 1, '2019-06-18 11:58:50', '2019-06-18 11:58:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_outcomes`
--

CREATE TABLE `history_outcomes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `outcome_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_input` date NOT NULL,
  `proof_of_transaction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `created_by_user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `image_activities`
--

CREATE TABLE `image_activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activity_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_type` tinyint(4) NOT NULL DEFAULT '0',
  `gender_type` tinyint(4) NOT NULL DEFAULT '0',
  `created_by_user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(34, '2019_05_06_142951_create_roles_table', 1),
(35, '2019_05_06_143027_create_donor_categories_table', 1),
(36, '2019_05_06_143108_create_activity_categories_table', 1),
(37, '2019_05_06_143157_create_donors_table', 1),
(38, '2019_05_06_143517_create_users_table', 1),
(39, '2019_05_06_143630_create_app_configs_table', 1),
(40, '2019_05_06_143706_create_activities_table', 1),
(41, '2019_05_06_143913_create_history_donors_table', 1),
(42, '2019_05_06_144108_create_members_table', 1),
(43, '2019_05_06_144143_create_history_outcomes_table', 1),
(44, '2019_05_06_154039_create_image_activities_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2019-05-22 16:33:26', '2019-05-22 16:33:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `is_active`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'bf5647f7e1ea7ecd077c9b07cef14ef9a1a9b5c7', 1, 1, '2019-05-22 16:33:26', '2019-05-22 16:33:26');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activities_activity_category_id_foreign` (`activity_category_id`),
  ADD KEY `activities_created_by_user_id_foreign` (`created_by_user_id`);

--
-- Indeks untuk tabel `activity_categories`
--
ALTER TABLE `activity_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `app_configs`
--
ALTER TABLE `app_configs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_configs_created_by_user_id_foreign` (`created_by_user_id`);

--
-- Indeks untuk tabel `donors`
--
ALTER TABLE `donors`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `donor_categories`
--
ALTER TABLE `donor_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `history_donors`
--
ALTER TABLE `history_donors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_donors_donor_category_id_foreign` (`donor_category_id`),
  ADD KEY `history_donors_donor_id_foreign` (`donor_id`),
  ADD KEY `history_donors_created_by_user_id_foreign` (`created_by_user_id`);

--
-- Indeks untuk tabel `history_outcomes`
--
ALTER TABLE `history_outcomes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_outcomes_created_by_user_id_foreign` (`created_by_user_id`);

--
-- Indeks untuk tabel `image_activities`
--
ALTER TABLE `image_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_activities_activity_id_foreign` (`activity_id`);

--
-- Indeks untuk tabel `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `members_created_by_user_id_foreign` (`created_by_user_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `activities`
--
ALTER TABLE `activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `activity_categories`
--
ALTER TABLE `activity_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `app_configs`
--
ALTER TABLE `app_configs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `donors`
--
ALTER TABLE `donors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `donor_categories`
--
ALTER TABLE `donor_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `history_donors`
--
ALTER TABLE `history_donors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `history_outcomes`
--
ALTER TABLE `history_outcomes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `image_activities`
--
ALTER TABLE `image_activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_activity_category_id_foreign` FOREIGN KEY (`activity_category_id`) REFERENCES `activity_categories` (`id`),
  ADD CONSTRAINT `activities_created_by_user_id_foreign` FOREIGN KEY (`created_by_user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `app_configs`
--
ALTER TABLE `app_configs`
  ADD CONSTRAINT `app_configs_created_by_user_id_foreign` FOREIGN KEY (`created_by_user_id`) REFERENCES `roles` (`id`);

--
-- Ketidakleluasaan untuk tabel `history_donors`
--
ALTER TABLE `history_donors`
  ADD CONSTRAINT `history_donors_created_by_user_id_foreign` FOREIGN KEY (`created_by_user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `history_donors_donor_category_id_foreign` FOREIGN KEY (`donor_category_id`) REFERENCES `donor_categories` (`id`),
  ADD CONSTRAINT `history_donors_donor_id_foreign` FOREIGN KEY (`donor_id`) REFERENCES `donors` (`id`);

--
-- Ketidakleluasaan untuk tabel `history_outcomes`
--
ALTER TABLE `history_outcomes`
  ADD CONSTRAINT `history_outcomes_created_by_user_id_foreign` FOREIGN KEY (`created_by_user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `image_activities`
--
ALTER TABLE `image_activities`
  ADD CONSTRAINT `image_activities_activity_id_foreign` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`id`);

--
-- Ketidakleluasaan untuk tabel `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_created_by_user_id_foreign` FOREIGN KEY (`created_by_user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
