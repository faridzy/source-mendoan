<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 14/02/19
 * Time: 03.53
 */

return[
    '200' => 'Data successfully saved!',
    '201' => 'Data available!',
    '404' => 'Data not available!',
    '302' => 'Data successfully updated!',
    '500' => 'An error occurred! Data failed to save! ',
    '202' => 'Data was successfully deleted!',
    '405' => 'An error occurred! Data failed to delete! ',
    '501' => 'Operation is not permitted! Data is used by other entities',
];