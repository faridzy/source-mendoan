<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Judul</label>
        <div class="col-sm-10">
            <input type="text" name="title" id="name" class="form-control form-control-sm" value="{{$data->title}}" required="">
        </div>
    </div>
    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Jenis Kegiatan</label>
        <div class="col-sm-10">
            <select class="form-control" name="activity_category_id">
                @foreach($categoryOption as $num => $item)
                    @if(isset($data->activity_category_id))
                        @if($data->activity_category_id==$item->id)
                            <option value="{{$item->id}}" selected="selected">{{$item->activity_category_name}}

                            </option>
                        @else
                            <option value="{{$item->id}}">{{$item->activity_category_name}}</option>
                        @endif
                    @else
                        <option value="{{$item->id}}">{{$item->activity_category_name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Foto</label>
        <div class="col-sm-8">
            <input type="file" name="foto" id="name" class="form-control form-control-sm">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Deskripsi</label>
        <div class="col-sm-10">
            <textarea name="description" class="form-control" rows="3" id="editor1" placeholder="Alamat">
                {!! $data->description !!}
            </textarea>
        </div>
    </div>


    <div class='form-group row'>
        <div class="col-md-2"></div>
        <div class='col-sm-10 '>
            <button class="btn btn-icon btn-3 btn-success btn-md" type="submit">
                <span class="btn-inner--icon"><i class="os-icon os-icon-save"></i></span>

                <span class="btn-inner--text">Simpan</span>

            </button>
        </div>
    </div>
    <input type='hidden' name='id' value='{{  encrypt($data->id) }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/activity/activity/save', data, '#result-form-konten');
        })
    })
    $('#datepicker').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    CKEDITOR.replace( 'editor1' );
</script>