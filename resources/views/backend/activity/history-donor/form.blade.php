<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Jenis Donasi</label>
        <div class="col-sm-10">
            <select class="form-control" name="donor_category_id">
                @foreach($donorCategoryOption as $num => $item)
                    @if(isset($data->donor_category_id))
                        @if($data->donor_category_id==$item->id)
                            <option value="{{$item->id}}" selected="selected">{{$item->donor_category_name}}

                            </option>
                        @else
                            <option value="{{$item->id}}">{{$item->donor_category_name}}</option>
                        @endif
                    @else
                        <option value="{{$item->id}}">{{$item->donor_category_name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label"> Donatur</label>
        <div class="col-sm-7">
            <select class="form-control" name="donor_id">
                @foreach($donorOption as $num => $item)
                    @if(isset($data->donor_id))
                        @if($data->donor_id==$item->id)
                            <option value="{{$item->id}}" selected="selected">{{$item->name}}

                            </option>
                        @else
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endif
                    @else
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col-sm-2">
            <a onclick="loadModal(this)" target="/backend/master/donor/add" class="btn btn-success btn-rounded" style="color: white" title="Tambah Data">
                <i class="os-icon os-icon-plus-circle"></i>Tambah Donatur</a>
        </div>

    </div>
    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label"> Tanggal Masuk</label>
        <div class="col-sm-5">
            <input class="form-control" placeholder="Tanggal" id="datepicker" name="date_input" type="text" value="{{$data->date_input}}" required>
        </div>

    </div>
    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Nominal</label>
        <div class="col-sm-10">
            <input type="text" name="amount" id="name" class="form-control form-control-sm" value="{{is_null($data->amount)?0:$data->amount}}" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Deskripsi</label>
        <div class="col-sm-10">
            <textarea name="description" class="form-control" rows="3" placeholder="Alamat">
                {{$data->description}}
            </textarea>
        </div>
    </div>



    <div class='form-group row'>
        <div class="col-md-2"></div>
        <div class='col-sm-10 '>
            <button class="btn btn-icon btn-3 btn-success btn-md" type="submit">
                <span class="btn-inner--icon"><i class="os-icon os-icon-save"></i></span>

                <span class="btn-inner--text">Simpan</span>

            </button>
        </div>
    </div>
    <input type='hidden' name='id' value='{{  encrypt($data->id) }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/activity/history-donor/save', data, '#result-form-konten');
        })
    })
    $('#datepicker').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
</script>