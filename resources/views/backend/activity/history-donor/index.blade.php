@extends('layout.main')
@section('title', $title)
@section('content')


    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Data Master</a></li>
        <li class="breadcrumb-item"><span>{{$title}}</span></li>
    </ul>
    <!--------------------
END - Breadcrumbs
-------------------->
    <div class="content-i">
        <div class="content-box">
            <div class="element-wrapper">
                <div class="element-box">
                    <div class="form-header">
                        <div class="row" style="margin-left: 0px; margin-right: 0px;">
                            <h5 class="col-lg-6">{{$title}}</h5>
                            <h5 class="col-lg-6" align="right">
                                <a onclick="loadModal(this)" target="/backend/activity/history-donor/add" class="btn btn-success btn-rounded" style="color: white" title="Tambah Data">
                                    <i class="os-icon os-icon-plus-circle"></i>Tambah Data</a></h5>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="datatable-buttons" width="100%" class="table table-striped table-lightfont">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Deskripsi</th>
                                <th>Nominal</th>
                                <th>Pendonor</th>
                                <th>Kategori Donor</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apakah anda yakin menghapus data ini ?", function () {
                ajaxTransfer("/backend/activity/history-donor/delete", data, "#modal-output");
            })
        }
        $(document).ready(function () {
            ajaxDataTable('#datatable-buttons',1, '/backend/activity/history-donor/data-table', [
                {data: 'rownum', name: 'rownum'},
                {data: 'date_input', name: 'date_input'},
                {data: 'description', name: 'description'},
                {data: 'amount', name: 'amount'},
                {data: 'name', name: 'name'},
                {data: 'donor_category_name', name: 'donor_category_name'},

                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]);
        });
    </script>
@endsection

