<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Nama Pengeluaran</label>
        <div class="col-sm-10">
            <input type="text" name="outcome_name" id="name" class="form-control form-control-sm" value="{{$data->outcome_name}}" required="">
        </div>
    </div>
    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label"> Tanggal Pengeluaran</label>
        <div class="col-sm-5">
            <input class="form-control" placeholder="Tanggal" id="datepicker" name="date_input" type="text" value="{{$data->date_input}}" required>
        </div>

    </div>
    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Nominal</label>
        <div class="col-sm-5">
            <input type="text" name="amount" id="name" class="form-control form-control-sm" value="{{is_null($data)?0:$data->amount}}" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Bukti Pengeluaran</label>
        <div class="col-sm-8">
            <input type="file" name="proof_of_transaction" id="name" class="form-control form-control-sm">
        </div>
    </div>


    <div class='form-group row'>
        <div class="col-md-2"></div>
        <div class='col-sm-10 '>
            <button class="btn btn-icon btn-3 btn-success btn-md" type="submit">
                <span class="btn-inner--icon"><i class="os-icon os-icon-save"></i></span>

                <span class="btn-inner--text">Simpan</span>

            </button>
        </div>
    </div>
    <input type='hidden' name='id' value='{{  encrypt($data->id) }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/activity/history-outcome/save', data, '#result-form-konten');
        })
    })
    $('#datepicker').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
</script>