@extends('layout.main')
@section('title', $title)
@section('content')


    <div class="content-w">
        <div class="content-i">
            <div class="content-box">
                <div class="element-wrapper compact pt-4">
                    <h6 class="element-header">Dashboard</h6>
                    <div class="element-box-tp">
                        <div class="row">
                            <div class="col-sm-5 col-xxl-4">
                                <div class="tablos">
                                    <div class="row mb-xl-2 mb-xxl-3">
                                        <div class="col-sm-6">
                                            <a class="element-box el-tablo centered trend-in-corner padded bold-label" style="height: 206px" href="#">
                                                <div class="value">{{$donorCount}}</div>
                                                <div class="label">Donatur</div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="element-box el-tablo centered trend-in-corner padded bold-label" style="height: 206px" href="#">
                                                <div class="value">{{$memberCount}}</div>
                                                <div class="label">Anggota</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a class="element-box el-tablo centered trend-in-corner padded bold-label" style="height: 206px" href="#">
                                                <div class="value">{{$historyDonorCount}}</div>
                                                <div class="label">Donasi</div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="element-box el-tablo centered trend-in-corner padded bold-label" style="height: 206px" href="#">
                                                <div class="value">{{$historyOutcomeCount}}</div>
                                                <div class="label">Pengeluaran</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7 col-xxl-8">
                                <!--START - Chart Box-->
                                <div class="element-box pl-xxl-5 pr-xxl-5">
                                    <div id="container" style="max-height: 400px"></div>
                                </div>
                                <!--END - Chart Box-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-xxl-12">
                        <!--START - CHART-->
                        <div class="element-wrapper">
                            <div class="element-box">
                                <div class="element-actions">

                                </div>
                                <h5 class="element-box-header">Donasi Terbaru</h5>
                                <div class="table-responsive">
                                    <table id="datatable-buttons"  width="100%" class="table table-striped table-lightfont">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Donatur</th>
                                            <th>Alamat</th>
                                            <th>No Telp</th>
                                            <th>Nominal</th>
                                            <th>Jenis Donasi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($donorLatest->count()>0)
                                        @foreach($donorLatest as $key =>$item)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$item->getDonor->name}}</td>
                                            <td>{{$item->getDonor->address}}</td>
                                            <td>{{$item->getDonor->phone_number}}</td>
                                            <td>{{number_format($item->amount,2)}}</td>
                                            <td>{{$item->getDonorCategory->donor_category_name}}</td>

                                        </tr>
                                       @endforeach
                                       @endif


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--END - CHART-->
                    </div>

                </div>
                <!--START - Transactions Table-->



                @endsection
                @section('scripts')
                    <script src="https://code.highcharts.com/highcharts.js"></script>
                    <script src="https://code.highcharts.com/modules/series-label.js"></script>
                    <script src="https://code.highcharts.com/modules/exporting.js"></script>
                    <script src="https://code.highcharts.com/modules/export-data.js"></script>
                    <script>
                        $(document).ready(function() {
                            $.ajax({
                                type: 'GET',
                                url: "{{url('backend/show-grafik')}}",
                                dataType: 'json',
                                success: function (data) {
                                    console.log(data);
                                   getData(data)
                                }
                            });
                        });
                    </script>
                    <script>
                        function getData(data) {
                            Highcharts.chart('container', {
                                chart: {
                                    type: 'line'
                                },
                                title: {
                                    text: 'Pemasukkan dan Pengeluaran Tahun {{date('Y')}}'
                                },
                                subtitle: {
                                    text: ''
                                },
                                xAxis: {
                                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                },
                                yAxis: {
                                    title: {
                                        text: 'Jumlah'
                                    }
                                },
                                plotOptions: {
                                    line: {
                                        dataLabels: {
                                            enabled: true
                                        },
                                        enableMouseTracking: false
                                    }
                                },
                                series: [{
                                    name: 'Pemasukkan',
                                    data: data.isIn
                                }, {
                                    name: 'Pengeluaran',
                                    data: data.isOut
                                }]
                            });
                        }
                    </script>

@endsection
