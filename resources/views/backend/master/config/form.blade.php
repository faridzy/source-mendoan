<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Nama Lembaga</label>
        <div class="col-sm-8">
            <input type="text" name="institution_name" id="name" class="form-control form-control-sm" value="{{$data->institution_name}}" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Sekilas Lembaga</label>
        <div class="col-sm-10">
            <textarea name="overview" class="form-control" rows="3" placeholder="Alamat">
                {{$data->overview}}
            </textarea>
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Logo</label>
        <div class="col-sm-8">
            <input type="file" name="logo" id="name" class="form-control form-control-sm">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">No Telp</label>
        <div class="col-sm-6">
            <input type="text" name="phone_number" id="name" class="form-control form-control-sm" value="{{$data->phone_number}}" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-7">
            <input type="email" name="email" id="name" class="form-control form-control-sm" value="{{$data->email}}" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Rekening Bank</label>
        <div class="col-sm-8">
            <input type="text" name="bank_number_rek" id="name" class="form-control form-control-sm" value="{{$data->bank_number_rek}}" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Alamat</label>
        <div class="col-sm-10">
            <textarea name="address" class="form-control" rows="3" placeholder="Alamat">
                {{$data->address}}
            </textarea>
        </div>
    </div>



    <div class='form-group row'>
        <div class="col-md-2"></div>
        <div class='col-sm-10 '>
            <button class="btn btn-icon btn-3 btn-success btn-md" type="submit">
                <span class="btn-inner--icon"><i class="os-icon os-icon-save"></i></span>

                <span class="btn-inner--text">Simpan</span>

            </button>
        </div>
    </div>
    <input type='hidden' name='id' value='{{  encrypt($data->id) }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/config/save', data, '#result-form-konten');
        })
    })
    $('#datepicker').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
</script>