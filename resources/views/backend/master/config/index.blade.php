@extends('layout.main')
@section('title', $title)
@section('content')


    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Data Master</a></li>
        <li class="breadcrumb-item"><span>{{$title}}</span></li>
    </ul>
    <!--------------------
END - Breadcrumbs
-------------------->
    <div class="content-i">
        <div class="content-box">
            <div class="element-wrapper">
                <div class="element-box">
                    <div class="form-header">
                        <div class="row" style="margin-left: 0px; margin-right: 0px;">
                            <h5 class="col-lg-6">{{$title}}</h5>
                            <h5 class="col-lg-6" align="right">
                                @if($data->count()<1)
                                <a onclick="loadModal(this)" target="/backend/master/config/add" class="btn btn-success btn-rounded" style="color: white" title="Tambah Data">
                                    <i class="os-icon os-icon-plus-circle"></i>Tambah Data</a>
                                 @endif
                            </h5>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="datatable-buttons"  width="100%" class="table table-striped table-lightfont">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Lembaga</th>
                                <th>Email</th>
                                <th>No Telp</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key =>$item)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->institution_name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->phone_number}}</td>
                                    <td>
                                        <a class='btn btn-sm btn-primary' onclick='loadModal(this)' target='/backend/master/config/add' data='id={{encrypt($item->id)}}' title='Edit'>
                                            <span class='os-icon os-icon-pencil-1' style='color: white'></span>
                                        </a>
                                        @if($key==0)
                                        @else
                                        <a class='btn btn-sm btn-danger' onclick=deleteData('{{encrypt($item->id)}}') title='Hapus'>
                                            <span class='os-icon os-icon-trash' style='color: white'></span>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apakah anda yakin menghapus data ini ?", function () {
                ajaxTransfer("/backend/master/config/delete", data, "#modal-output");
            })
        }
    </script>
@endsection

