<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Nama Kategori</label>
        <div class="col-sm-10">
            <input type="text" name="donor_category_name" id="name" class="form-control form-control-sm" value="{{$data->donor_category_name}}" required="">
        </div>
    </div>


    <div class='form-group row'>
        <div class="col-md-2"></div>
        <div class='col-sm-10 '>
            <button class="btn btn-icon btn-3 btn-success btn-md" type="submit">
                <span class="btn-inner--icon"><i class="os-icon os-icon-save"></i></span>

                <span class="btn-inner--text">Simpan</span>

            </button>
        </div>
    </div>
    <input type='hidden' name='id' value='{{  encrypt($data->id) }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/donor-category/save', data, '#result-form-konten');
        })
    })
</script>