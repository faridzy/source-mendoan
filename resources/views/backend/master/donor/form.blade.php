<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Nama Donatur</label>
        <div class="col-sm-10">
            <input type="text" name="name" id="name" class="form-control form-control-sm" value="{{$data->name}}" required="">
        </div>
    </div>
    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Jenis Kelamin</label>
        <div class="col-sm-10">
            <div class="form-check"><label class="form-check-label">
                    <input  class="form-check-input" name="gender_type" type="radio" value="1" @if($data->gender_type==1) checked @endif>
                    Pria</label>
            </div>
            <br>
            <div class="form-check"><label class="form-check-label">
                    <input  class="form-check-input" name="gender_type" type="radio" value="2" @if($data->gender_type==2) checked @endif>
                    Wanita</label>
            </div>

        </div>
    </div>
    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">No Telp</label>
        <div class="col-sm-10">
            <input type="text" name="phone_number" id="name" class="form-control form-control-sm" value="{{$data->phone_number}}" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Alamat</label>
        <div class="col-sm-10">
            <textarea name="address" class="form-control" rows="3" placeholder="Detail Transaksi">
                {{$data->address}}
            </textarea>
        </div>
    </div>


    <div class='form-group row'>
        <div class="col-md-2"></div>
        <div class='col-sm-10 '>
            <button class="btn btn-icon btn-3 btn-success btn-md" type="submit">
                <span class="btn-inner--icon"><i class="os-icon os-icon-save"></i></span>

                <span class="btn-inner--text">Simpan</span>

            </button>
        </div>
    </div>
    <input type='hidden' name='id' value='{{  encrypt($data->id) }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/donor/save', data, '#result-form-konten');
        })
    })
</script>