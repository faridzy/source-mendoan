<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Nama Pengguna</label>
        <div class="col-sm-10">
            <input type="text" name="username" id="name" class="form-control form-control-sm" value="{{$data->username}}" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
            <input type="password" name="password" id="name" class="form-control form-control-sm" required="">
        </div>
    </div>

    <div class="form-group row">
        <label for='role_name' class="col-sm-2 col-form-label">Hak Akses</label>
        <div class="col-sm-10">
           <select class="form-control" name="role_id">
               <option value="0">Hak Akses</option>
               @foreach($roleOption as $num => $item)
                   @if(isset($data->role_id))
                       @if($data->role_id==$item->id)
                           <option value="{{$item->id}}" selected="selected">{{$item->role_name}}

                           </option>
                       @else
                           <option value="{{$item->id}}">{{$item->role_name}}</option>
                       @endif
                   @else
                       <option value="{{$item->id}}">{{$item->role_name}}</option>
                   @endif
               @endforeach
           </select>
        </div>
    </div>


    <div class='form-group row'>
        <div class="col-md-2"></div>
        <div class='col-sm-10 '>
            <button class="btn btn-icon btn-3 btn-success btn-md" type="submit">
                <span class="btn-inner--icon"><i class="os-icon os-icon-save"></i></span>

                <span class="btn-inner--text">Simpan</span>

            </button>
        </div>
    </div>
    <input type='hidden' name='id' value='{{  encrypt($data->id) }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/user/save', data, '#result-form-konten');
        })
    })
</script>