   <table style="border:1px solid #000;border-collapse:collapse">
        <tr style="border:1px solid #000">
            <td><b>Laporan Bulan </b></td>
            <td><b>{{generateMonthName($month)}} {{$year}}</b></td>
        </tr>
    </table>
    <table style="border:1px solid #000;border-collapse:collapse">
        <tr style="border:1px solid #000;">
            <td>No</td>
            <td>Tanggal</td>
            <td>Nama Pemasukan</td>
            <td>Jumlah</td>
            <td>Nama Pengeluaran</td>
            <td>Jumlah</td>

        </tr>
        <tr style="border:1px solid #000;">
            <td>1</td>
            <td>-</td>
            <td><b>Saldo Bulan Lalu</b></td>
            <td><b>{{number_format($saldo,2)}}</b></td>
            <td></td>
            <td></td>

        </tr>
        @foreach($data as $key =>$item)
            @if($item['type']==0)
                <tr style="border:1px solid #000;">
                    <td>{{$key+2}}</td>
                    <td>{{$item['date_input']}}</td>
                    <td>{{$item['description']}}</td>
                    <td>{{number_format($item['amount'],2)}}</td>
                    <td>-</td>
                    <td>-</td>

                </tr>
            @else
                <tr style="border:1px solid #000;">
                <td>{{$key+2}}</td>
                <td>{{$item['date_input']}}</td>
                <td>-</td>
                <td>-</td>
                <td>{{$item['description']}}</td>
                <td>{{number_format($item['amount'],2)}}</td>
                </tr>


            @endif
        @endforeach

        <tr style="border:1px solid #000;">
            <td></td>
            <td>-</td>
            <td><b>Total Pemasukkan</b></td>
            <td><b>{{($in->count()>0)?(number_format($in[0]['sums']+$saldo,2)):'-'}}</b></td>
            <td><b>Total Pengeluaran</b></td>
            <td><b>{{($out->count()>0)?number_format($out[0]['sums'],2):'-'}}</b></td>

        </tr>
    </table>

