@extends('layout.main')
@section('title', $title)
@section('content')


    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Data Master</a></li>
        <li class="breadcrumb-item"><span>{{$title}}</span></li>
    </ul>
    <!--------------------
END - Breadcrumbs
-------------------->
    <div class="content-i">
        <div class="content-box">
            <div class="element-wrapper">
                <div class="element-box">
                    <div class="form-header">
                        <div class="row" style="margin-left: 0px; margin-right: 0px;">
                            <h5 class="col-lg-6">{{$title}}</h5>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

                            <div class="form-group row">
                                <label for='role_name' class="col-sm-2 col-form-label">Cetak Report</label>
                                <div class="col-sm-3">
                                    <select name="month" class="form-control" required>
                                        <option value="1">Januari</option>
                                        <option value="2">Februari</option>
                                        <option value="3">Maret</option>
                                        <option value="4">April</option>
                                        <option value="5">Mei</option>
                                        <option value="6">Juni</option>
                                        <option value="7">Juli</option>
                                        <option value="8">Agustus</option>
                                        <option value="9">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select name="year" class="form-control" required>
                                        @for($i=date('Y');$i<=2100;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                                    <button class="btn btn-icon btn-3 btn-success btn-md" type="submit">
                                        <span class="btn-inner--icon"><i class="os-icon os-icon-search"></i></span>

                                        <span class="btn-inner--text">Cari</span>

                                    </button>

                                </div>
                            </div>


                        </form>
                    </div>
                </div>

                <br><br>
                <div style="clear: both"></div>
                <div id="results"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $("#form-konten").submit(function () {
            var data = getFormData("form-konten");
            ajaxTransfer("/backend/report/report-monthly/show", data, function (result) {
                $("#results").html(result);
                $("#table-data").dataTable();
            });
        })
    </script>
@endsection

