
            <div class="element-box">
                <div class="form-header">
                    <div class="row" style="margin-left: 0px; margin-right: 0px;">
                        <h5 class="col-lg-6">Laporan Bulan {{generateMonthName($month)}} {{$year}}</h5>
                    </div>
                </div>
                <div class="table-responsive">
                    @if(count($data)>0)
                    <table id="datatable-buttons"  width="100%" class="table table-striped table-lightfont">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nama Pemasukan</th>
                            <th>Jumlah</th>
                            <th>Nama Pengeluaran</th>
                            <th>Jumlah</th>

                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>1</td>
                            <td>-</td>
                            <td><b>Saldo Bulan Lalu</b></td>
                            <td><b>{{number_format($saldo,2)}}</b></td>
                            <td></td>
                            <td></td>

                        </tr>
                        @foreach($data as $key =>$item)
                            @if($item['type']==0)
                        <tr>
                            <td>{{$key+2}}</td>
                            <td>{{$item['date_input']}}</td>
                            <td>{{$item['description']}}</td>
                            <td>{{number_format($item['amount'],2)}}</td>
                            <td>-</td>
                            <td>-</td>

                        </tr>
                        @else
                                <tr>
                                <td>{{$key+2}}</td>
                                <td>{{$item['date_input']}}</td>
                                <td>-</td>
                                <td>-</td>
                                <td>{{$item['description']}}</td>
                                <td>{{number_format($item['amount'],2)}}</td>

                                </tr>
                            @endif
                        @endforeach

                        <tr>
                            <td></td>
                            <td>-</td>
                            <td><b>Total Pemasukkan</b></td>
                            <td><b>{{($in->count()>0)?(number_format($in[0]['sums']+$saldo,2)):'-'}}</b></td>
                            <td><b>Total Pengeluaran</b></td>
                            <td><b>{{($out->count()>0)?number_format($out[0]['sums'],2):'-'}}</b></td>

                        </tr>

                        </tbody>
                    </table>
                        <div class="row">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <a href="{{url('backend/report/report-monthly/excel?month='.$month.'&year='.$year.'')}}" class="btn btn-icon btn-3 btn-primary btn-md">
                                    <i class="os-icon os-icon-printer"></i>
                                    Cetak Excel</a>

                            </div>
                        </div>
                    @else
                    <div class="alert alert-warning">Data tidak tersedia</div>
                    @endif
                </div>
            </div>