<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{env('APP_NAME')}}| @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/frontend/')}}/img/favicon.png">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/normalize.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/main.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/bootstrap.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/animate.min.css">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/font-awesome.min.css">
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/vendor/OwlCarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.theme.default.min.css">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/meanmenu.min.css">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/jquery.datetimepicker.css">
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/magnific-popup.css">
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/hover-min.css">
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/css/reImageGrid.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/')}}/style.css">
    <!-- Modernizr Js -->
    <script src="{{asset('public/frontend/')}}/js/modernizr-2.8.3.min.js"></script>
</head>

<body>
@php $data=getConfig() @endphp

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Add your site or application content here -->
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<!-- Main Body Area Start Here -->
<div id="wrapper">
    <!-- Header Area Start Here -->
    @include('layout-frontend.menu')
    <!-- Header Area End Here -->
    @yield('content')
    <!-- Footer Area Start Here -->
    <footer>
        <div class="footer-area-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="footer-box">
                            <a href="index.html"><img class="img-responsive" src="{{asset('public/frontend/')}}/img/logo-footer.png" alt="logo"></a>
                            <div class="footer-about">
                                <p>
                                    {{(!is_null($data))?$data->overview:''}}
                                </p>
                            </div>

                            <ul class="footer-social">
                                <li><a href="index.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                                <li><a href="index.html#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="footer-box">
                            <h3>Featured Links</h3>
                            <ul class="featured-links">
                                <li>
                                    <ul>
                                        <li><a href="index.html#">Kegiatan Terbaru</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <ul>
                                        <li><a href="index.html#">Donasi</a></li>

                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

                        <div class="footer-box">
                            <h3>Information</h3>
                            <ul class="corporate-address">
                                <li><i class="fa fa-phone" aria-hidden="true"></i><a href="#"> {{(!is_null($data))?$data->phone_number:''}} </a></li>
                                <li><i class="fa fa-envelope-o" aria-hidden="true"></i>{{(!is_null($data))?$data->email:''}}</li>
                            </ul>
                            <div class="newsletter-area">
                                <h3>Newsletter</h3>
                                <div class="input-group stylish-input-group">
                                    <input type="text" placeholder="Enter your e-mail here" class="form-control">
                                    <span class="input-group-addon">
                                                <button type="submit">
                                                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                                </button>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-area-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <p>&copy; 2019  All Rights Reserved. &nbsp; Designed by<a target="_blank" href="#"> Mendoan Team</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Area End Here -->
</div>
<!-- Main Body Area End Here -->
<!-- jquery-->
<script src="{{asset('public/frontend/')}}/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<!-- Plugins js -->
<script src="{{asset('public/frontend/')}}/js/plugins.js" type="text/javascript"></script>
<!-- Bootstrap js -->
<script src="{{asset('public/frontend/')}}/js/bootstrap.min.js" type="text/javascript"></script>
<!-- WOW JS -->
<script src="{{asset('public/frontend/')}}/js/wow.min.js"></script>
<!-- Nivo slider js -->
<script src="{{asset('public/frontend/')}}/vendor/slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="{{asset('public/frontend/')}}/vendor/slider/home.js" type="text/javascript"></script>
<!-- Owl Cauosel JS -->
<script src="{{asset('public/frontend/')}}/vendor/OwlCarousel/owl.carousel.min.js" type="text/javascript"></script>
<!-- Meanmenu Js -->
<script src="{{asset('public/frontend/')}}/js/jquery.meanmenu.min.js" type="text/javascript"></script>
<!-- Srollup js -->
<script src="{{asset('public/frontend/')}}/js/jquery.scrollUp.min.js" type="text/javascript"></script>
<!-- jquery.counterup js -->
<script src="{{asset('public/frontend/')}}/js/jquery.counterup.min.js"></script>
<script src="{{asset('public/frontend/')}}/js/waypoints.min.js"></script>
<!-- Countdown js -->
<script src="{{asset('public/frontend/')}}/js/jquery.countdown.min.js" type="text/javascript"></script>
<!-- Isotope js -->
<script src="{{asset('public/frontend/')}}/js/isotope.pkgd.min.js" type="text/javascript"></script>
<!-- Magic Popup js -->
<script src="{{asset('public/frontend/')}}/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<!-- Gridrotator js -->
<script src="{{asset('public/frontend/')}}/js/jquery.gridrotator.js" type="text/javascript"></script>
<!-- Custom Js -->
<script src="{{asset('public/frontend/')}}/js/main.js" type="text/javascript"></script>
</body>

</html>
