<header>
    <div id="header1" class="header1-area">
        <div class="main-menu-area bg-primary" id="sticker">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-3">
                        <div class="logo-area">
                            <a href="index.html"><img class="img-responsive" src="{{asset('public/frontend/')}}/img/logo.png" alt="logo"></a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-9">
                        <nav id="desktop-nav">
                            <ul>
                                <li><a href="{{url('/')}}">Home</a>
                                </li>
                                <li><a href="#">Kegiatan Terbaru</a>
                                </li>
                                <li><a href="#">Donasi</a>
                                </li>


                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-2 col-md-2 hidden-sm">
                        <div class="apply-btn-area">
                            <a href="{{url('/login')}}" class="apply-now-btn">Masuk</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu Area Start -->
    <div class="mobile-menu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li><a href="{{url('/')}}">Home</a>
                                </li>
                                <li><a href="#">Kegiatan Terbaru</a>
                                </li>
                                <li><a href="#">Donasi</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu Area End -->
</header>