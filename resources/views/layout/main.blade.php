<!DOCTYPE html>
<html>

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="{{asset('public/backend/')}}/favicon.png" rel="shortcut icon">
    <link href="{{asset('public/backend/')}}/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="http://fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/')}}/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/css/main-version=4.4.1.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/backend/')}}/core/ajax.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="{{asset('public/backend/')}}/bower_components/jquery/dist/jquery.min.js"></script>

    <style>
        /* CSS used here will be applied after bootstrap.css */

        .modal{
            display: block !important;
        }
        .modal-dialog{
            overflow-y: initial !important
        }
        .modal-body{
            max-height: 450px;
            overflow-y: auto;
        }
    </style>

</head>
<body class="menu-position-side menu-side-left with-content-panel">
<div class="all-wrapper with-side-panel solid-bg-all">
    {{--<div aria-hidden="true" class="onboarding-modal modal fade animated show-on-load" role="dialog" tabindex="-1">--}}
        {{--<div class="modal-dialog modal-centered" role="document">--}}
            {{--<div class="modal-content text-center">--}}
                {{--<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span class="close-label">Skip Intro</span><span class="os-icon os-icon-close"></span></button>--}}
                {{--<div class="onboarding-slider-w">--}}
                    {{--<div class="onboarding-slide">--}}
                        {{--<div class="onboarding-media"><img alt="" src="{{asset('public/backend/')}}/img/bigicon2.png" width="200px"></div>--}}
                        {{--<div class="onboarding-content with-gradient">--}}
                            {{--<h4 class="onboarding-title">Example of onboarding screen!</h4>--}}
                            {{--<div class="onboarding-text">This is an example of a multistep onboarding screen, you can use it to introduce your customers to your app, or collect additional information from them before they start using your app.</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="onboarding-slide">--}}
                        {{--<div class="onboarding-media"><img alt="" src="{{asset('public/backend/')}}/img/bigicon5.png" width="200px"></div>--}}
                        {{--<div class="onboarding-content with-gradient">--}}
                            {{--<h4 class="onboarding-title">Example Request Information</h4>--}}
                            {{--<div class="onboarding-text">In this example you can see a form where you can request some additional information from the customer when they land on the app page.</div>--}}
                            {{--<form>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="">Your Full Name</label>--}}
                                            {{--<input class="form-control" placeholder="Enter your full name..." type="text" value="">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-6">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="">Your Role</label>--}}
                                            {{--<select class="form-control">--}}
                                                {{--<option>Web Developer</option>--}}
                                                {{--<option>Business Owner</option>--}}
                                                {{--<option>Other</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="onboarding-slide">--}}
                        {{--<div class="onboarding-media"><img alt="" src="{{asset('public/backend/')}}/img/bigicon6.png" width="200px"></div>--}}
                        {{--<div class="onboarding-content with-gradient">--}}
                            {{--<h4 class="onboarding-title">Showcase App Features</h4>--}}
                            {{--<div class="onboarding-text">In this example you can showcase some of the features of your application, it is very handy to make new users aware of your hidden features. You can use boostrap columns to split them up.</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<ul class="features-list">--}}
                                        {{--<li>Fully Responsive design</li>--}}
                                        {{--<li>Pre-built app layouts</li>--}}
                                        {{--<li>Incredible Flexibility</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<ul class="features-list">--}}
                                        {{--<li>Boxed & Full Layouts</li>--}}
                                        {{--<li>Based on Bootstrap 4</li>--}}
                                        {{--<li>Developer Friendly </li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="search-with-suggestions-w">
        <div class="search-with-suggestions-modal">
            <div class="element-search">
                <input class="search-suggest-input" placeholder="Start typing to search..." type="text">
                <div class="close-search-suggestions"><i class="os-icon os-icon-x"></i></div>
                </input>
            </div>
            {{--<div class="search-suggestions-group">--}}
                {{--<div class="ssg-header">--}}
                    {{--<div class="ssg-icon">--}}
                        {{--<div class="os-icon os-icon-box"></div>--}}
                    {{--</div>--}}
                    {{--<div class="ssg-name">Projects</div>--}}
                    {{--<div class="ssg-info">24 Total</div>--}}
                {{--</div>--}}
                {{--<div class="ssg-content">--}}
                    {{--<div class="ssg-items ssg-items-boxed">--}}
                        {{--<a class="ssg-item" href="users_profile_big.html">--}}
                            {{--<div class="item-media" style="background-image: url('{{asset('public/backend/')}}/img/company6.png')"></div>--}}
                            {{--<div class="item-name">Integ<span>ration</span> with API</div>--}}
                        {{--</a>--}}
                        {{--<a class="ssg-item" href="users_profile_big.html">--}}
                            {{--<div class="item-media" style="background-image: url('{{asset('public/backend/')}}/img/company7.png')"></div>--}}
                            {{--<div class="item-name">Deve<span>lopm</span>ent Project</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="search-suggestions-group">--}}
                {{--<div class="ssg-header">--}}
                    {{--<div class="ssg-icon">--}}
                        {{--<div class="os-icon os-icon-users"></div>--}}
                    {{--</div>--}}
                    {{--<div class="ssg-name">Customers</div>--}}
                    {{--<div class="ssg-info">12 Total</div>--}}
                {{--</div>--}}
                {{--<div class="ssg-content">--}}
                    {{--<div class="ssg-items ssg-items-list">--}}
                        {{--<a class="ssg-item" href="users_profile_big.html">--}}
                            {{--<div class="item-media" style="background-image: url('{{asset('public/backend/')}}/img/avatar1.jpg')"></div>--}}
                            {{--<div class="item-name">John Ma<span>yer</span>s</div>--}}
                        {{--</a>--}}
                        {{--<a class="ssg-item" href="users_profile_big.html">--}}
                            {{--<div class="item-media" style="background-image: url('{{asset('public/backend/')}}/img/avatar2.jpg')"></div>--}}
                            {{--<div class="item-name">Th<span>omas</span> Mullier</div>--}}
                        {{--</a>--}}
                        {{--<a class="ssg-item" href="users_profile_big.html">--}}
                            {{--<div class="item-media" style="background-image: url('{{asset('public/backend/')}}/img/avatar3.jpg')"></div>--}}
                            {{--<div class="item-name">Kim C<span>olli</span>ns</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="search-suggestions-group">--}}
                {{--<div class="ssg-header">--}}
                    {{--<div class="ssg-icon">--}}
                        {{--<div class="os-icon os-icon-folder"></div>--}}
                    {{--</div>--}}
                    {{--<div class="ssg-name">Files</div>--}}
                    {{--<div class="ssg-info">17 Total</div>--}}
                {{--</div>--}}
                {{--<div class="ssg-content">--}}
                    {{--<div class="ssg-items ssg-items-blocks">--}}
                        {{--<a class="ssg-item" href="index.html#">--}}
                            {{--<div class="item-icon"><i class="os-icon os-icon-file-text"></i></div>--}}
                            {{--<div class="item-name">Work<span>Not</span>e.txt</div>--}}
                        {{--</a>--}}
                        {{--<a class="ssg-item" href="index.html#">--}}
                            {{--<div class="item-icon"><i class="os-icon os-icon-film"></i></div>--}}
                            {{--<div class="item-name">V<span>ideo</span>.avi</div>--}}
                        {{--</a>--}}
                        {{--<a class="ssg-item" href="index.html#">--}}
                            {{--<div class="item-icon"><i class="os-icon os-icon-database"></i></div>--}}
                            {{--<div class="item-name">User<span>Tabl</span>e.sql</div>--}}
                        {{--</a>--}}
                        {{--<a class="ssg-item" href="index.html#">--}}
                            {{--<div class="item-icon"><i class="os-icon os-icon-image"></i></div>--}}
                            {{--<div class="item-name">wed<span>din</span>g.jpg</div>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="ssg-nothing-found">--}}
                        {{--<div class="icon-w"><i class="os-icon os-icon-eye-off"></i></div><span>No files were found. Try changing your query...</span></div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="layout-w">
       @include('layout.menu')
        <div class="content-w">
            <div class="top-bar color-scheme-transparent">
                <div class="top-menu-controls">
                    <div class="element-search autosuggest-search-activator">
                        <input placeholder="Start typing to search..." type="text">
                    </div>
                    <div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-left"><i class="os-icon os-icon-ui-46"></i>
                        <div class="os-dropdown">
                            <div class="icon-w"><i class="os-icon os-icon-ui-46"></i></div>
                            <ul>
                                <li><a href="#"><i class="os-icon os-icon-ui-49"></i><span>Detail Profil</span></a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="logged-user-w">
                        <div class="logged-user-i">
                            <div class="avatar-w"><img alt="" src="{{asset('public/backend/')}}/img/avatar1.jpg"></div>
                            <div class="logged-user-menu color-style-bright">
                                <div class="logged-user-avatar-info">
                                    <div class="avatar-w"><img alt="" src="{{asset('public/backend/')}}/img/avatar1.jpg"></div>
                                    <div class="logged-user-info-w">
                                        <div class="logged-user-name">{{Session::get('activeUser')->username}}</div>
                                        <div class="logged-user-role">{{Session::get('activeUser')->getRole->role_name}}</div>
                                    </div>
                                </div>
                                <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                                <ul>
                                    <li><a href="#"><i class="os-icon os-icon-user-male-circle2"></i><span>Detail Profil</span></a></li>
                                    <li><a href="{{url('/logout')}}"><i class="os-icon os-icon-signs-11"></i><span>Keluar</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           @yield('content')
        </div>
    </div>
</div>
<script src="{{asset('public/backend/')}}/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/moment/moment.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/chart.js/dist/Chart.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap-validator/dist/validator.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/dropzone/dist/dropzone.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/editable-table/mindmup-editabletable.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/tether/dist/js/tether.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/slick-carousel/slick/slick.min.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/util.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/alert.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/button.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/carousel.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/collapse.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/dropdown.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/modal.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/tab.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/tooltip.js"></script>
<script src="{{asset('public/backend/')}}/bower_components/bootstrap/js/dist/popover.js"></script>
<script src="{{asset('public/backend/')}}/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('public/backend/')}}/js/demo_customizer-version=4.4.1.js"></script>
<script src="{{asset('public/backend/')}}/js/main-version=4.4.1.js"></script>
<input type='hidden' name='_token' value='{{ csrf_token() }}'>

<script src="{{asset('public/backend/core/ajax.js')}}"></script>
<script src="{{asset('public/backend/core/datatable.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{asset('public/backend/core/ckeditor/ckeditor.js')}}"></script>

@yield('scripts')
<script>
    baseURL = '{{url("/")}}';
</script>
</body>
</html>