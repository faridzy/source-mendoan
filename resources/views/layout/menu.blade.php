<!--------------------
START - Mobile Menu
-------------------->
<div class="menu-mobile menu-activated-on-click color-scheme-dark">
    <div class="mm-logo-buttons-w">
        <a class="mm-logo" href="#"><img src="{{asset('public/backend/')}}/img/logo.png"><span>Mendoan Apps</span></a>
        <div class="mm-buttons">
            <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles"></div>
            </div>
            <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
            </div>
        </div>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <div class="avatar-w"><img alt="" src="{{asset('public/backend/')}}/img/avatar1.jpg"></div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">{{Session::get('activeUser')->username}}</div>
                <div class="logged-user-role">{{Session::get('activeUser')->getRole->role_name}}</div>
            </div>
        </div>
        <!--------------------
START - Mobile Menu List
-------------------->
        <ul class="main-menu">
            <li class="has-sub-menu">
                <a href="{{url('/backend')}}">/<div class="icon-w">
                        <div class="os-icon os-icon-layout"></div>
                    </div><span>Dashboard</span></a>
            </li>
            <li class=" has-sub-menu">
                <a href="#">
                    <div class="icon-w">
                        <div class="os-icon os-icon-layers"></div>
                    </div><span>Data Master</span></a>
                <div class="sub-menu-w">
                    <div class="sub-menu-header">Data Master</div>
                    <div class="sub-menu-icon"><i class="os-icon os-icon-layers"></i></div>
                    <div class="sub-menu-i">
                        <ul class="sub-menu">
                            <li><a href="{{url('backend/master/role')}}">Hak Akses</a></li>
                            <li><a href="{{url('backend/master/user')}}">Pengguna</a></li>
                            <li><a href="{{url('backend/master/donor')}}">Donatur</a></li>
                            <li><a href="{{url('backend/master/member')}}">Anggota/Member</a></li>
                        </ul>
                        <ul class="sub-menu">
                            <li><a href="{{url('backend/master/donor-category')}}">Kategori Donasi</a></li>
                            <li><a href="{{url('backend/master/activity-category')}}">Kategori Kegiatan</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class="sub-header"><span>Aktivitas</span></li>
            <li class=" has-sub-menu">
                <a href="#">
                    <div class="icon-w">
                        <div class="os-icon os-icon-package"></div>
                    </div><span>Aktivitas</span></a>
                <div class="sub-menu-w">
                    <div class="sub-menu-header">Aktivitas</div>
                    <div class="sub-menu-icon"><i class="os-icon os-icon-package"></i></div>
                    <div class="sub-menu-i">
                        <ul class="sub-menu">
                            <li><a href="{{url('backend/activity/history-donor')}}">Pemasukan</a></li>
                            <li><a href="{{url('backend/activity/history-outcome')}}">Pengeluaran</a></li>
                            <li><a href="{{url('backend/activity/activity')}}">Kegiatan</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li class=" has-sub-menu">
                <a href="#">
                    <div class="icon-w">
                        <div class="os-icon os-icon-file-text"></div>
                    </div><span>Report</span></a>
                <div class="sub-menu-w">
                    <div class="sub-menu-header">Report</div>
                    <div class="sub-menu-icon"><i class="os-icon os-icon-file-text"></i></div>
                    <div class="sub-menu-i">
                        <ul class="sub-menu">
                            <li><a href="#">Laporan Pemasukan</a></li>
                            <li><a href="#">Laporan Pengeluaran</a></li>
                            <li><a href="#">Rekap Data</a></li>
                        </ul>

                    </div>
                </div>
            </li>
        </ul>
        <!--------------------
END - Mobile Menu List
-------------------->

    </div>
</div>
<!--------------------
END - Mobile Menu
-------------------->
<!--------------------
START - Main Menu
-------------------->
    <div class="menu-w selected-menu-color-light color-schema-dark menu-activated-on-hover menu-has-selected color-scheme-dark color-style-light sub-menu-color-bright menu-position-side menu-side-left menu-layout-full sub-menu-style-over">
    <div class="logo-w">
        <a class="logo" href="#">
            <div class="logo-element"></div>
            <div class="logo-label">Mendoan Apps</div>
        </a>
    </div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w"><img alt="" src="{{asset('public/backend/')}}/img/avatar1.jpg"></div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">{{Session::get('activeUser')->username}}</div>
                <div class="logged-user-role">{{Session::get('activeUser')->getRole->role_name}}</div>
            </div>
            <div class="logged-user-toggler-arrow">
                <div class="os-icon os-icon-chevron-down"></div>
            </div>
            <div class="logged-user-menu color-style-bright">
                <div class="logged-user-avatar-info">
                    <div class="avatar-w"><img alt="" src="{{asset('public/backend/')}}/img/avatar1.jpg"></div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">{{Session::get('activeUser')->username}}</div>
                        <div class="logged-user-role">{{Session::get('activeUser')->getRole->role_name}}</div>
                    </div>
                </div>
                <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                <ul>
                    <li><a href="{{url('/logout')}}"><i class="os-icon os-icon-signs-11"></i><span>Keluar</span></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="element-search autosuggest-search-activator">
        <input placeholder="Start typing to search..." type="text">
    </div>
    <h1 class="menu-page-header">Page Header</h1>
    <ul class="main-menu">
        <li class="sub-header"><span>Dashboard</span></li>
        <li class="selected has-sub-menu">
            <a href="{{url('/')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div><span>Dashboard</span></a>
            <div class="sub-menu-w">
                <div class="sub-menu-header">Dashboard</div>
                <div class="sub-menu-icon"><i class="os-icon os-icon-layout"></i></div>
                <div class="sub-menu-i">
                    <ul class="sub-menu">
                        <li><a href="{{url('/')}}">Dashboard</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class=" has-sub-menu">
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-layers"></div>
                </div><span>Data Master</span></a>
            <div class="sub-menu-w">
                <div class="sub-menu-header">Data Master</div>
                <div class="sub-menu-icon"><i class="os-icon os-icon-layers"></i></div>
                <div class="sub-menu-i">
                    <ul class="sub-menu">
                        <li><a href="{{url('backend/master/role')}}">Hak Akses</a></li>
                        <li><a href="{{url('backend/master/user')}}">Pengguna</a></li>
                        <li><a href="{{url('backend/master/donor')}}">Donatur</a></li>
                        <li><a href="{{url('backend/master/member')}}">Anggota/Member</a></li>
                    </ul>
                    <ul class="sub-menu">
                        <li><a href="{{url('backend/master/donor-category')}}">Kategori Donasi</a></li>
                        <li><a href="{{url('backend/master/activity-category')}}">Kategori Kegiatan</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="sub-header"><span>Aktivitas</span></li>
        <li class=" has-sub-menu">
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div><span>Aktivitas</span></a>
            <div class="sub-menu-w">
                <div class="sub-menu-header">Aktivitas</div>
                <div class="sub-menu-icon"><i class="os-icon os-icon-package"></i></div>
                <div class="sub-menu-i">
                    <ul class="sub-menu">
                        <li><a href="{{url('backend/activity/history-donor')}}">Pemasukan</a></li>
                        <li><a href="{{url('backend/activity/history-outcome')}}">Pengeluaran</a></li>
                        <li><a href="{{url('backend/activity/activity')}}">Kegiatan</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class=" has-sub-menu">
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-file-text"></div>
                </div><span>Report</span></a>
            <div class="sub-menu-w">
                <div class="sub-menu-header">Report</div>
                <div class="sub-menu-icon"><i class="os-icon os-icon-file-text"></i></div>
                <div class="sub-menu-i">
                    <ul class="sub-menu">
                        <li><a href="{{url('backend/report/report-monthly')}}">Laporan Per Bulan</a></li>
                    </ul>

                </div>
            </div>
        </li>
        <li>
            <a href="{{url('backend/master/config')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-settings"></div>
                </div><span>Setting</span></a>
        </li>

    </ul>

</div>
<!--------------------
END - Main Menu
-------------------->