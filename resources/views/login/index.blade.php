<!DOCTYPE html>
<html>

<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="{{asset('public/backend/')}}/favicon.png" rel="shortcut icon">
    <link href="{{asset('public/backend/')}}/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="http://fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet" type="text/css">
    <link href="{{asset('public/backend/')}}/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="{{asset('public/backend/')}}/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/backend/')}}/core/ajax.css" type="text/css">
    <link href="{{asset('public/backend/')}}/css/main-version=4.4.1.css" rel="stylesheet">
    <script src="{{asset('public/backend/')}}/bower_components/jquery/dist/jquery.min.js"></script>
</head>

<body class="auth-wrapper">
<div class="all-wrapper menu-side with-pattern">
    <div class="auth-box-w">
        <div class="logo-w">
            <a href="#"><img alt="" src="{{asset('public/backend/')}}/img/logo-big.png"></a>
        </div>
        <h4 class="auth-header">Login Form</h4>
        <form role="form" action="" onsubmit="return false" id="form-konten">
            <div id="results"></div>
            <div class="form-group">
                <label for="">Username</label>
                <input class="form-control" name="username" placeholder="Username" type="text">
                <div class="pre-icon os-icon os-icon-user-male-circle"></div>
            </div>
            <div class="form-group">
                <label for="">Password</label>
                <input class="form-control" placeholder="Username" name="password" type="password">
                <div class="pre-icon os-icon os-icon-fingerprint"></div>
            </div>
            <div class="buttons-w">
                <button class="btn btn-primary" type="submit">Log me in</button>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox">Remember Me</label>
                </div>
            </div>
        </form>
    </div>
</div>
<input type='hidden' name='_token' value='{{ csrf_token() }}'>
<script src="{{asset('public/backend/core/ajax.js')}}"></script>
<script type="text/javascript">
    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{url('/validate-login')}}", data, '#results');
    });

    function redirectPage(){
        redirect(1000, '/backend');
    }
</script>
</body>

</html>