<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//frontend
Route::get('/', 'Frontend\FrontendController@index');


//backend
Route::get('login', 'LoginController@index');
Route::get('logout', 'LoginController@logout');
Route::post('validate-login', 'LoginController@validateLogin');
Route::group(['prefix' => 'backend', 'namespace' => 'Backend','middleware'=> ['login-verification']], function () {
    Route::get('/', 'BackendController@index');
    Route::get('/show-grafik', 'BackendController@showGrafik');
    Route::group(['prefix' => 'master', 'namespace' => 'Master'], function () {
        Route::group(['prefix' => 'role'], function () {
            Route::get('/', 'RoleController@index');
            Route::post('/add', 'RoleController@add');
            Route::post('/save', 'RoleController@save');
            Route::post('/delete', 'RoleController@delete');
            Route::post('/data-table', 'RoleController@dataTable');
        });
        Route::group(['prefix' => 'donor'], function () {
            Route::get('/', 'DonorController@index');
            Route::post('/add', 'DonorController@add');
            Route::post('/save', 'DonorController@save');
            Route::post('/delete', 'DonorController@delete');
            Route::post('/data-table', 'DonorController@dataTable');
        });
        Route::group(['prefix' => 'config'], function () {
            Route::get('/', 'AppConfigController@index');
            Route::post('/add', 'AppConfigController@add');
            Route::post('/save', 'AppConfigController@save');
            Route::post('/delete', 'AppConfigController@delete');
        });
        Route::group(['prefix' => 'user'], function () {
            Route::get('/', 'UserController@index');
            Route::post('/add', 'UserController@add');
            Route::post('/save', 'UserController@save');
            Route::post('/delete', 'UserController@delete');
            Route::post('/data-table', 'UserController@dataTable');
        });
        Route::group(['prefix' => 'member'], function () {
            Route::get('/', 'MemberController@index');
            Route::post('/add', 'MemberController@add');
            Route::post('/save', 'MemberController@save');
            Route::post('/delete', 'MemberController@delete');
            Route::post('/data-table', 'MemberController@dataTable');
        });
        Route::group(['prefix' => 'activity-category'], function () {
            Route::get('/', 'ActivityCategoryController@index');
            Route::post('/add', 'ActivityCategoryController@add');
            Route::post('/save', 'ActivityCategoryController@save');
            Route::post('/delete', 'ActivityCategoryController@delete');
            Route::post('/data-table', 'ActivityCategoryController@dataTable');
        });
        Route::group(['prefix' => 'donor-category'], function () {
            Route::get('/', 'DonorCategoryController@index');
            Route::post('/add', 'DonorCategoryController@add');
            Route::post('/save', 'DonorCategoryController@save');
            Route::post('/delete', 'DonorCategoryController@delete');
            Route::post('/data-table', 'DonorCategoryController@dataTable');
        });
    });
    Route::group(['prefix' => 'activity', 'namespace' => 'Activity'], function () {
        Route::group(['prefix' => 'activity'], function () {
            Route::get('/', 'ActivityController@index');
            Route::post('/add', 'ActivityController@add');
            Route::post('/save', 'ActivityController@save');
            Route::post('/delete', 'ActivityController@delete');
            Route::post('/data-table', 'ActivityController@dataTable');
        });
        Route::group(['prefix' => 'history-donor'], function () {
            Route::get('/', 'HistoryDonorController@index');
            Route::post('/add', 'HistoryDonorController@add');
            Route::post('/save', 'HistoryDonorController@save');
            Route::post('/delete', 'HistoryDonorController@delete');
            Route::post('/data-table', 'HistoryDonorController@dataTable');
        });
        Route::group(['prefix' => 'history-outcome'], function () {
            Route::get('/', 'HistoryOutcomeController@index');
            Route::post('/add', 'HistoryOutcomeController@add');
            Route::post('/save', 'HistoryOutcomeController@save');
            Route::post('/delete', 'HistoryOutcomeController@delete');
            Route::post('/data-table', 'HistoryOutcomeController@dataTable');
        });
    });
    Route::group(['prefix' => 'report', 'namespace' => 'Report'], function () {
        Route::group(['prefix' => 'report-monthly'], function () {
            Route::get('/', 'ReportController@index');
            Route::post('/show', 'ReportController@show');
            Route::get('/excel', 'ReportController@excel');


        });
    });
});